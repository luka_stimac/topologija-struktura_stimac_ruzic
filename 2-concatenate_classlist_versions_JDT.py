# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 22:57:45 2017

@author: intel
"""
#Nazivi datoteka koje će biti učitane
filenames = ['generic_classlist/JDT_classlist/1-remove_class_ID_numbers/2_0_BEZ_ID.classlist', 'generic_classlist/JDT_classlist/1-remove_class_ID_numbers/2_1_BEZ_ID.classlist','generic_classlist/JDT_classlist/1-remove_class_ID_numbers/3_0_BEZ_ID.classlist',
'generic_classlist/JDT_classlist/1-remove_class_ID_numbers/3_1_BEZ_ID.classlist','generic_classlist/JDT_classlist/1-remove_class_ID_numbers/3_2_BEZ_ID.classlist','generic_classlist/JDT_classlist/1-remove_class_ID_numbers/3_3_BEZ_ID.classlist',
'generic_classlist/JDT_classlist/1-remove_class_ID_numbers/3_4_BEZ_ID.classlist','generic_classlist/JDT_classlist/1-remove_class_ID_numbers/3_5_BEZ_ID.classlist','generic_classlist/JDT_classlist/1-remove_class_ID_numbers//3_6_BEZ_ID.classlist',
'generic_classlist/JDT_classlist/1-remove_class_ID_numbers/3_7_BEZ_ID.classlist','generic_classlist/JDT_classlist/1-remove_class_ID_numbers/3_8_BEZ_ID.classlist','generic_classlist/JDT_classlist/1-remove_class_ID_numbers/4_2_BEZ_ID.classlist',
'generic_classlist/JDT_classlist/1-remove_class_ID_numbers/4_3_BEZ_ID.classlist' ]

#Otvori datoteku u koju ćemo spojiti classliste iz svih verzija
with open('concatenated_classlist.txt', 'w') as outfile:
#Iteriraj kroz datoteke
    for fname in filenames:
        with open(fname) as infile:
#Iteriraj kroz redove unutar datoteke i zapiši ih u spojenu classlistu
            for line in infile:                                
                    outfile.write(line)

					
            
