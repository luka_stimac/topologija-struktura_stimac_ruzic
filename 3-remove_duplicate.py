# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 23:15:22 2017

@author: intel
"""
#Set redova koji su već viđeni u drugim datotekama
lines_seen = set() 

#Otvori datoteku u koju ćemo pisati
outfile = open('concatenated_classlist_removed_duplicate.txt', "w")

#Iteriraj kroz prijašnje kreiranu datoteku koja sadrži classliste svih verzija
for line in open('generic_classlist/JDT_classlist/2-concatenate_classlist_versions_JDT/concatenated_classlist.txt', "r"):
#Provjeri postoji li trenutni red u setu već viđenih
    if line not in lines_seen: 
#Ako ne postoji u setu viđenih, dodaj ga u set i zapiši u datoteku bez duplikata
        outfile.write(line)
        lines_seen.add(line)

#Zatvori datoteku
outfile.close()
