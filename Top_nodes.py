# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 21:35:18 2017

@author: luka
"""
import numpy as np

clique_list = open('Clique/JDT/4_3/all_clique_with_id.txt','r')
linije_clique=clique_list.readlines()
clique_list.close()

nid_classlist = open('generic_classlist/JDT_classlist/orig_files/4_3.classlist','r')
linije_nid = nid_classlist.readlines()
nid_classlist.close()

#Kreiranje matrice

matrica=np.zeros((len(linije_clique),int(linije_nid[-1].split('-')[0])))
top=np.zeros(int(linije_nid[-1].split('-')[0]))

print np.shape(matrica)

for i in range(len(linije_clique)):
    #Dohvaćanje čvorova unutar clique-a
    clique=linije_clique[i].split('>')[1].replace(' ','').replace('\n','').split(',')
    for j in range(int(linije_nid[-1].split('-')[0])):  
        for k in range(len(clique)):
            #Pretraga je li čvor dio clique-a i upisivanje jedinice u matricu ako je
            if int(clique[k])==int(linije_nid[j].split('-')[0]):
                matrica[i][j]=1
                top[j]=top[j]+1
                #print str(clique) + " " + linije_nid[j].split('-')[0]
                break
            else:
                continue

print top

#Datoteka u koju se spremaju najčešći čvorovi, treba paziti na direktorije putanje unutar Home direktorija
najcesce=open('Najčešći_čvorovi/JDT/Top-4_3.txt','w')

#Zapiši header u datoteku
najcesce.write("ID | Class Name | Broj pojavljivanja\n\n")

#Pronađi 5 čvorova koji se najviše pojavljuju u clique-ovima
top_tri= sorted(range(len(top)), key=lambda i: top[i])[-5:]

#Formatiraj podatke i zapiši u datoteku
for i in reversed(xrange(len(top_tri))):
    top_tri[i]=top_tri[i]+1
    for j in range(int(linije_nid[-1].split('-')[0])):
        if top_tri[i]==int(linije_nid[j].split('-')[0]):                
            najcesce.write(str(top_tri[i])+' | '+linije_nid[j].split('>')[1].replace('\n','') +' | N='+str(int(top[int(top_tri[i])-1]))+'\n')
            
najcesce.close()

#print matrica[2]
np.savetxt("test.csv", matrica, delimiter=",")
        
