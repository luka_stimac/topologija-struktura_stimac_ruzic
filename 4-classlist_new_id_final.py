# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 23:24:27 2017

@author: intel
"""

with open('all_clique.txt', 'r') as program:
    data = program.readlines()

with open('all_clique_with_id.txt', 'w') as program:
    for (number, line) in enumerate(data):
        program.write('%d->%s' % (number + 1, line))
        