# -*- coding: utf-8 -*-
"""
Created on Tue May  2 17:21:45 2017
@author: intel
"""
#CHANGE THIS PARAMETERS FOR DIFFERENT VERSIONS OF PROJECT
all_clique='input_for_our_script/all_clique.txt'
lookup_table='input_for_our_script/Lookup-4_3.txt'
output_file_for_perseus='perseus_4_3_input.txt'

import os
#open nongeneric clique id files for every version 
clique_list = open(all_clique,'r')
linije_clique=clique_list.readlines()
clique_list.close()

#opne file in which is stored lookup table for every version
lookup_list = open(lookup_table,'r')
linije_lookup = lookup_list.readlines()
lookup_list.close()

#open file in which we will temporary save the generic clique structure
f_out = open('dont_change.txt', 'w')
#define string
generic_clique=''
#define counter ans set it to 0
n=0
for i in range(len(linije_clique)):      
    #save cliques in 'clique' list
    clique=linije_clique[i].replace(' ','').replace('\n','').split(',')
    for j in range(len(linije_lookup)):
        for k in range(len(clique)):
        #find cliques in lookup table
            if int(clique[k])==int(linije_lookup[j].split(' ')[0]):
                #if is not the last element, then add it to string
                if(n<len(clique)-1):
                    #save generic id from lookup table to string                    
                    generic_clique=generic_clique+linije_lookup[j].split(' ')[1].replace('\n','') + ' '
                    #increase counter                  
                    n+=1
                    break
                else:
                    #save generic id from lookup table to string                    
                    generic_clique=generic_clique+linije_lookup[j].split(' ')[1]
                    #write  'generic_clique' to file                    
                    f_out.write(generic_clique)
                    #empty the string after every write to file                    
                    generic_clique=''
                    #set the counter to 0 after every write to file                     
                    n=0
                    break
            else:
                continue
  
#Close open files
f_out.close()

#this part of the code will compute birth date, size of clique and write 1(undirected graph) on the top of the file 
#open previously stored data of generic clique structure
clique_list = open('dont_change.txt','r')
linije_clique=clique_list.readlines()
clique_list.close()   

#open a file in which we will save the final clique structure
f_out = open(output_file_for_perseus, 'w')         
#write 1 on the top of the file
f_out.write('1\n')
#make a list with ones, size is the number of lines in file(clique structure)
#each number represents the initial birth time,the index of the list are clique ID
number = 1
number2 = 0
elements = len(linije_clique)
lista = [number] * elements
#make a list in which we will delete the non maximl cliques and the ones that are not part of any other clique
clique_temp=[number2] * elements        
#counter for deleted items
deleted=0

for i in range(len(linije_clique)):
    #save the splited clique components in list for K clique   
    line_i=linije_clique[i].replace('\n','').split(' ')
    for j in range(len(linije_clique)):
        #do not compare same lines        
        if(i!=j):
            #save the splited clique components in list for K+1 clique        
            line_j=(linije_clique[j].replace('\n','').split(' '))        
            #delete all the elements that are not connected to others
            for n in range(len(linije_clique[i].split(' '))):                       
                for m in range(len(linije_clique[j].split(' '))):
                    #if clique components are the same,mark as good                    
                    if(line_i[n]==line_j[m]):                                      
                        clique_temp[i]=1
                        break

for i in range(len(linije_clique)):
    #save the split clique components in list for K clique   
    line_i=linije_clique[i].replace('\n','').split(' ')
    for j in range(len(linije_clique)):
        #save the splited clique components in list for K+1 clique        
        line_j=(linije_clique[j].replace('\n','').split(' '))        
        #compare cliques that are different in only 1 size(compare K clique only with K+1 clique)
        if(len(linije_clique[i].split(' '))==int(len(linije_clique[j].split(' ')))-1):
            #set the counter to 0 for every new clique                     
            suma=0            
            for n in range(len(linije_clique[i].split(' '))):
                for m in range(len(linije_clique[j].split(' '))):
                    #if clique components are the same                    
                    if(line_i[n]==line_j[m]):
                        #increase counter                        
                        suma+=1
                        #if all K-cliques are found in K+1-cliques                        
                        if(suma==(int(len(linije_clique[j].split(' ')))-1)):
                            #increase for 1 the previous birt date where the K clique was maximal and write it to the location of the K+1 clique               
                            lista[j]=lista[i]+1
                            #put 0 where are located the non maximal cliques                            
                            clique_temp[i]=0
                                          
for i in range(len(linije_clique)):
    #if a clique is not maximal do not store it in the output file
    if(clique_temp[i]!=0):    
        #save clique to list    
        clique=linije_clique[i].replace('\n','').split(' ')
        #at the begining of the string add the size of the clique,then the clique structure and at the end the birth time
        generic_clique=str(len(clique)-1)+' '+linije_clique[i].replace('\n','')+' '+str(lista[i])+'\n'
        #write string to file    
        f_out.write(generic_clique)
        #empty the string after every write to file                    
        generic_clique=''
    else:
        deleted+=1
               
print 'Deleted items:',deleted      
#remove temporary file 'dont_change.txt'    
os.remove('dont_change.txt')     
#close open files and write Finished
f_out.close()
print "Finished!"



