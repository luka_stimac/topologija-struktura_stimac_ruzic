# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 21:35:18 2017

@author: luka
"""
import numpy as np

#Učitaj listu clique struktura i spremi sve retke datoteke u polje linije_clique
clique_list = open('Clique/JDT/4_3/all_clique_with_id.txt','r')
linije_clique=clique_list.readlines()
clique_list.close()

#Učitaj originalnu class listu i spremi sve retke datoteke u polje linije_nid
nid_classlist = open('generic_classlist/JDT_classlist/0-original_files/4_3.classlist','r')
linije_nid = nid_classlist.readlines()
nid_classlist.close()

#Kreiraj matricu i ispuni ju nulama, te ispisi njene dimenzije
matrica=np.zeros((len(linije_clique),int(linije_nid[-1].split('-')[0])))
print np.shape(matrica)

for i in range(len(linije_clique)):
    #Dohvati čvorove unutar clique-a, i spremi ih u polje clique
    clique=linije_clique[i].split('>')[1].replace(' ','').replace('\n','').split(',')
    for j in range(int(linije_nid[-1].split('-')[0])):  
        for k in range(len(clique)):
            #Pretraži je li čvor dio clique-a i upiši jedinicu u matricu ako je
            if int(clique[k])==int(linije_nid[j].split('-')[0]):
                matrica[i][j]=1
                break
            else:
                continue

#Spremi matricu u csv formatu
np.savetxt("Matrica/JDT/4_3.csv", matrica, delimiter=",")
        
