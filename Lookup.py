# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 10:57:30 2017

@author: luka
"""
#Otvori datoteku koja sadrži originalnu classlistu za pojedinu verziju ("nid" je node id)
nid_classlist = open('generic_classlist/PDE_classlist/0-original_files/4_3.classlist','r')
linije_nid=nid_classlist.readlines()
nid_classlist.close()

#Otvori datoteku koja sadrži generičnu classlistu koja obuhvaća sve klase ("gid" je generic id)
gid_classlist = open('generic_classlist/PDE_classlist/4-classlist_new_id_final/classlist_new_id_final.txt','r')
linije_gid = gid_classlist.readlines()
gid_classlist.close()

#Otvori datoteku u kojoj ćemo stvoriti lookup vrijednosti za dohvat generičnog id iz node id i obrnuto
f_out = open('Lookup/PDE/Lookup-4_3.txt', 'w')

#Iteriraj kroz redove datoteka node id i generic id
for i in range(len(linije_nid)):
    for j in range(len(linije_gid)):

#Izdvoji dio retka svake datoteke koji sadrži naziv klase i usporedi nazive
        if ''.join(linije_nid[i].split('>')[1:]) == ''.join(linije_gid[j].split('>')[1:]):
            #print linije_nid[i].split('-')[0] + " " + linije_gid[j].split('-')[0]+'\n'
#Ako su nazivi isti zapiši u prvom stupcu lookup datoteke node id vrijednost, a u drugom generic id
            f_out.write(linije_nid[i].split('-')[0] + " " + linije_gid[j].split('-')[0]+'\n')
            break
        else:
            continue

#Zatvori datoteku u koju smo zapisivali i ispiši notifikaciju "Gotovo!"
f_out.close()
print "Gotovo!"

